CREATE TABLE IF NOT EXISTS test (id PRIMARY KEY, name TEXT);
SELECT crsql_as_crr('test');

CREATE VIRTUAL TABLE test_fts
USING fts5 (id, name);

INSERT INTO test_fts (id, name)
SELECT
    id, name
FROM
    test;

CREATE TRIGGER insert_test_fts
    AFTER INSERT on test
BEGIN
    INSERT INTO test_fts (id, name)
    VALUES (NEW.id, NEW.name);
END;

CREATE TRIGGER update_test_fts
    AFTER UPDATE ON test
BEGIN    
    UPDATE test_fts
    SET
        name = NEW.name
    WHERE id = NEW.id;

END;

CREATE TRIGGER delete_test_fts
    AFTER DELETE ON test
BEGIN
    DELETE FROM test_fts
    WHERE id = OLD.id;
END;