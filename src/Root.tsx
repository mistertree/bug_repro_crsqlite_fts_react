import { DBProvider } from "@vlcn.io/react";
import App from "./App.tsx";
import schemaContent from "./schemas/main.sql?raw";

const dbname = "main"

export default function Root() {  
  return (
    <DBProvider
      dbname={dbname}
      schema={{
        name: "main.sql",
        content: schemaContent,
      }}
      Render={() => <App dbname={dbname} />}
    />
  );
}
